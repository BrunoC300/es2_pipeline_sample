import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestHelloWorld {

    @Test
    public void test_sayHello(){
        assertEquals("Hello World", new HelloWorld().sayHello());
    }
}
